<?php if($data&&$iconPath)?>
<iframe id="demo" class="row10 col12" allowfullscreen="" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Display a popup on click</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:0; bottom:0; width:100%; }
    </style>
</head>
<body>


<style>
    .mapboxgl-popup {
        max-width: 400px;
        font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
    }
    .marker {
        display: block;
        border: none;
        cursor: pointer;
        padding: 0;
    }
</style>

<div id='map' style=" "></div>
<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoibXVuaXIxMzU3IiwiYSI6ImNqbnU1dWJ4dzBnYWoza3J3ZXdmcjZ6ZzAifQ.jqf5sLoUrIjQ70YloZ1ttg';

    var geojson={
            "type": "FeatureCollection",
            "features": [
                @foreach($data as $item)
                {
                "type": "Feature",
                "properties": {
                    "description": "<div><h3><b>{!!  empty($item->location->label)?$item->company->name: $item->location->label !!}</b></h3><br>{!!  empty($item->address)?'':$item->address  !!} <br> @if(!empty($item->location->info_json)) @set($arr,$item->location->info_json) @for($i=0;$i<=count($arr)/2;$i++) @if(!empty($arr['text'.$i])){!! preg_replace('/\r\n?|\n/', '', nl2br(e($arr['text'.$i])))  !!} @endif @endfor @endif  </div>",
                    "iconSize": [40, 40],
                    'iconPath':' {{ !isset($iconPath[$item->id])?asset('storage/'.$iconPath[0]): asset('storage/'.$iconPath[$item->id]) }}'
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [{{ empty($item->location->longitude)?'null':$item->location->longitude }}, {{ empty($item->location->latitude)?'null':$item->location->latitude }}]
                }
                },
                @endforeach
            ]
        };
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [-74.005974, 40.712776],
        zoom: 11
    });
    geojson.features.forEach(function(marker) {
        // create the popup
        var popup = new mapboxgl.Popup({ offset: 25 })
            .setHTML(marker.properties.description);

        // create a DOM element for the marker
        var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage = 'url(' + marker.properties.iconPath + '/)';
        el.style.backgroundSize= 'contain';
        el.style.backgroundRepeat= 'no-repeat';
        el.style.backgroundPosition= '50% 50%';
        el.style.width = marker.properties.iconSize[0] + 'px';
        el.style.height = marker.properties.iconSize[1] + 'px';

        // add marker to map
        new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(popup) // sets a popup on this marker
            .addTo(map);
    });
</script>

</body>
</html>

</iframe>
