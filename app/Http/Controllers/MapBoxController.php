<?php

namespace App\Http\Controllers;

use App\ComDep;
use App\ComService;
use App\Location;
use App\ServiceType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapBoxController extends Controller
{
    public function index($city_id){
        $idDepWCity=ComDep::select('id')->where('com_city_id',$city_id)->pluck('id');
        $idDepWCity->all();
        if(empty($idDepWCity)) return back();
        $defaultIcon=ServiceType::find(env('DEFAULT_ICON_SERVICE_TYPE_ID'))->img_icon_path;
        $idManyServiceTypeOne=ComService::select('com_dep_id')->whereIn('com_dep_id',$idDepWCity)->groupBy('com_dep_id','service_type_id')->pluck('com_dep_id');
        $idDefService=$this->getComDepIdSD($idManyServiceTypeOne->all());
        $iconService=ComService::select('com_dep_id','service_type_id')->whereIn('com_dep_id',$idDepWCity)->groupBy('com_dep_id','service_type_id')->pluck('com_dep_id');
        $iconService=$this->getComDepIdSIP($iconService->all(),$idDefService);
        $iconService=ComService::select('com_dep_id','service_type_id')->whereIn('com_dep_id',$iconService)->get();
        $serviceTypes=ServiceType::all('id','img_icon_path');
        $iconPath=[];
        $idDepSelect=[];
        foreach ($serviceTypes as $item){
            $iconPath[$item->id]=$item->img_icon_path;
        }
        $idDepIconPath=['0'=>$defaultIcon];
        foreach ($iconService as $item){
            $idDepIconPath[$item->com_dep_id]=$iconPath[$item->service_type_id];
            array_push($idDepSelect,$item->com_dep_id);
        }
        $iconService=ComService::select('com_dep_id','service_type_id')->whereIn('com_dep_id',$idDefService)->get();
        foreach ($iconService as $item){
            if(!array_search($item->com_dep_id,$idDepSelect)) {
                array_push($idDepSelect, $item->com_dep_id);
                $idDepIconPath[$item->com_dep_id] = $defaultIcon;

            }
        }
        $data=ComDep::all()->where('com_city_id',$city_id);
        $data->load('location','company');
//        foreach ($data as $item){
//            if(empty($item->location->info_json)||count($item->location->info_json)<1):
//                echo $item->address.'<br>';
//            else:
//                $arr = $item->location->info_json;
//                for($i=1;$i<=count($arr)/2;$i++):
//                    if(!empty($arr['text'.($i)])){
//                        echo preg_replace('/\r\n?|\n/', '', nl2br(e($arr['text'.($i)])));
//                    }
//                endfor;
//            endif;
//        }
//        dd();
        return view('map')->with([
            'data'=>$data,
            'iconPath'=>$idDepIconPath,
            ])->render();
    }

    public function getComDepIdSD($array){
        $arrayCount=[0];
        $arRet=[];
        foreach ($array as $item){
            if(array_search($item,$arrayCount))array_push($arRet,$item);
            array_push($arrayCount,$item);
        }
        return $arRet;
    }

    public function getComDepIdSIP($array,$arrayNot){
        $arRet=[];
        foreach ($array as $item){
            $l=0;
            foreach ($arrayNot as $i){
                if($i*1==$item*1) $l++;
            }
            if($l==0)array_push($arRet, $item);
        }
        return $arRet;
    }
    //
}
