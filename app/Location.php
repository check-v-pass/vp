<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    protected $casts = [
        'info_json' => 'json',
    ];
    use SoftDeletes;
//    public $guarded=['id'];
    protected $fillable =['info_json','info','longitude','latitude','id_admin_del','id_admin_add','created_at','updated_at','deleted_at','label','com_dep_id'];
    public function com_dep(){
        return $this->belongsTo(ComDep::class);
    }
    //
}
