<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $casts = [
        'src' => 'json',
    ];

    //
}
