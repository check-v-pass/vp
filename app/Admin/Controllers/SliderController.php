<?php

namespace App\Admin\Controllers;

use App\Company;
use \App\Slider;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Slider);

        $grid->id('Id');
        $grid->company_id('company')->display(function ($id){
            return Company::find($id)->name;
        });
        $grid->src('Src');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Slider::findOrFail($id));

        $show->id('Id');
        $show->company_id('Id company');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Slider);

        $form->select('company_id', 'Company')->options(Company::all()->pluck('name','id'));
        $form->embeds('src','Select image',function ($form){
            $form->html('<input type="file" name="min" class="">');
        });
        $form->saving(function (Form $form){
            try{
                $srcmin='images/slider/'.md5(uniqid()).'.jpg';
                Image::make(Input::file('min'))->resize(300, 200)->encode('jpg')->save(public_path('storage/'.$srcmin));
                $srcmax='images/slider/'.md5(uniqid()).'.jpg';
                Image::make(Input::file('min'))->resize(700, 400)->encode('jpg')->save(public_path('storage/'.$srcmax));

                $form->src=['min'=>$srcmin,'max'=>$srcmax];
            }catch (Exception $e){
                $this->er_message=$this->er_message.'Error in image upload and resize';
                return $this->return_error($this->er_message);
            }
        });
        return $form;
    }

    public $er_message='';
    public function return_error($message){
        $error = new MessageBag([
            'title'   => 'ERROR',
            'message' => $message,
        ]);
        return back()->with(compact('error'))->withInput();
    }
}
