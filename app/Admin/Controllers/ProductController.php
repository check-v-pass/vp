<?php

namespace App\Admin\Controllers;

use App\Product;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Part;
use Encore\Admin\Facades\Admin;
use App\ComCity;
use Illuminate\Support\MessageBag;

class ProductController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product);

        $grid->id('Id');
        $grid->name('Name');
        $grid->info('Info');
        $grid->price_adult('Price adult');
        $grid->price_child('Price child');
        $grid->part_id('Part')->display(function($id) {
            return Part::find($id)->name;
        });
        $grid->city_id('City')->display(function($id_cities) {
            return ComCity::find($id_cities)->name;
        });
        $grid->days_active('Days active');
        $grid->days_after_activation('Days after activation');
        $grid->attraction('Attraction');
        $grid->limit_com_services('Limit company services');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->info('Info');
        $show->price_adult('Price adult');
        $show->price_child('Price child');
        $show->id_admin_add('Id admin add');
        $show->part_id('Part id');
        $show->city_id('Id city');
        $show->days_active('Days active');
        $show->days_after_activation('Days after activation');
        $show->attraction('Attraction');
        $show->limit_com_services('Limit com services');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $this->opt();
        $form = new Form(new Product);

        $form->text('name', 'Name')->rules('required|min:3|max:200');
        $form->textarea('info', 'Info')->rules('nullable');
        $form->select('part_id', 'Part')->options($this->optpart)->rules('required|exists:parts,id');
        $form->number('days_active', 'Days active')->default(360);
        $form->number('days_after_activation', 'Days after activation')->default(0);
        $form->switch('limit_com_services', 'Limit company services for attraction mode');$form->html('<br>');
        $form->number('attraction', 'Attraction');
        $form->select('city_id', trans('company_cities'))->options($this->optionCity)->rules('required|exists:com_cities,id');
        $form->currency('price_adult', 'Price adult')->symbol('$');
        $form->currency('price_child', 'Price child')->symbol('$');
        $form->saving(function (Form $form) {
            if($form->limit_com_services){
                if($form->attraction*1<=0){
                    $this->er_message=$this->er_message.'Attraction count';
                    return $this->return_error($this->er_message);
                }
                $form->days_after_activation=env('ACTIVE_ATTRACTION');
            }
            $form->model()->id_admin_add=Admin::user()->id;

        });
        return $form;
    }
    public $optpart,$optionCity;
    public function opt(){
        if(empty($this->optpart))$this->optpart=Part::all()->pluck('name','id');
        if(empty($this->optionCity)) $this->optionCity=ComCity::all()->pluck('name', 'id');
    }
    public $er_message='';
    public function return_error($message){
        $error = new MessageBag([
            'title'   => 'ERROR',
            'message' => $message,
        ]);
        return back()->with(compact('error'))->withInput();
    }
}
