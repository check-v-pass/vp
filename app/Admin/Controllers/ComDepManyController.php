<?php

namespace App\Admin\Controllers;

use App\ComCity;
use App\ComDep;
use \App\Company;
use App\ComService;
use App\Http\Controllers\Controller;
use App\ServiceType;
use App\Slider;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\MessageBag;

class ComDepManyController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Company::class, function (Grid $grid) {

            $grid->disableCreateButton();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });


            $grid->id('Id');
            $grid->name('Company name');
            $grid->info_for_vp_admin('Info for vp admin');
            $grid->web_site('Web site');
            $grid->e_mail('E mail');
            $grid->com_deps('Offices count')->display(function ($comments) {
                $count = count($comments);
                return "<span class='label label-warning'>{$count}</span>";
            });

        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->deleted_at('Deleted at');
        $show->id_admin_del('Id admin del');
        $show->id_admin_add('Id admin add');
        $show->info('Info');
        $show->info_for_vp_admin('Info for vp admin');
        $show->id_com_admin('Id com admin');
        $show->telephone('Telephone');
        $show->web_site('Web site');
        $show->e_mail('E mail');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public $er_message='';
    protected function form()
    {
        $form = new Form(new Company);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
            $tools->disableDelete();
        });

        $form->display('name', 'Company name');
        $form->hasMany('com_deps', function (Form\NestedForm $form) {
            $form->text('name', 'Office name')->rules('required|min:3|max:239');
            $form->select('id_com_dep_admin', 'Company department admin')->options(Admin::user()->all()->pluck('name', 'id'))->rules('required|exists:admin_users,id');
            $form->select('com_city_id', trans('company_cities'))->options(ComCity::all()->pluck('name', 'id'))->rules('required|exists:com_cities,id');
            $form->text('address', 'Address')->rules('required|min:3|max:239');
            $form->textarea('info_station', 'Info station')->rules('nullable');
            $form->mobile('telephone', 'Telephone')->rules('nullable|unique:com_deps,telephone');
            $form->email('e_mail', 'E mail')->rules('nullable|unique:com_deps,e_mail|max:190');
            $form->text('class', 'Class')->rules('nullable');
            $form->textarea('info', 'Info')->rules('nullable');;
            $form->textarea('info_for_vp_admin', 'Info for vp admin')->rules('nullable');;

        });


        return $form;
    }

    public function return_error($message){

    }
}