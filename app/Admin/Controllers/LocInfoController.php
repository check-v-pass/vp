<?php

namespace App\Admin\Controllers;

use App\ComDep;
use App\Location;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LocInfoController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Location);

        $grid->id('Id');
        $grid->com_dep_id('Company office')->display(function ($id) {
            return ComDep::find($id)->name;
        });
        $grid->label('Label');
        $grid->info_json('Info json');
        $grid->latitude('Latitude');
        $grid->longitude('Longitude');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Location::findOrFail($id));

        $show->id('Id');
        $show->com_dep_id('Com dep id');
        $show->label('Label');
        $show->info_json('Info json');
        $show->latitude('Latitude');
        $show->longitude('Longitude');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->id_admin_add('Id admin add');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Location);

        $form->text('label', 'Label')->rules('max:240');
        $form->embeds('info_json', function ($form) {
            $form->icon('icon1')->default('fa-bus');
            $form->text('text1')->rules('max:240');

            $form->icon('icon2')->default('fa-subway');
            $form->text('text2')->rules('max:240');

            $form->icon('icon3')->default('fa-info');
            $form->text('text3')->rules('max:240');
        });
        $form->display('latitude', 'Latitude');
        $form->display('longitude', 'Longitude');

        return $form;
    }
}
