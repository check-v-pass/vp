<?php

namespace App\Admin\Controllers;

use \App\ComDep;
use App\Company;
use App\Http\Controllers\Controller;
use App\Location;
use App\ServiceType;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ComKMController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $lat=40.75236;
        $lng=-73.977412;
        $latmax=$lat+env('LAT_1KM')*1;
        $latmin=$lat-env('LAT_1KM')*1;
        $lngmin=$lng-env('LNG_1km')*1;
        $lngmax=$lng+env('LNG_1km')*1;

        $cl=Location::all()->where('latitude', '<', $latmax)->where('latitude','>',$latmin)->where('longitude','<>',$lng)->where('latitude','<>',$lat)->where('longitude', '<', $lngmax)->where('longitude','>',$lngmin);
        dump($cl);
        return Admin::grid(ComDep::class, function (Grid $grid) {
            $grid->disableCreateButton();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->id('Id');
            $grid->company_id('Company')->display(function ($id) {
                return Company::find($id)->name;
            });
            $grid->name('Department');
            $grid->service_type_id('Service type')->display(function ($id) {
                return ServiceType::find($id)->name;
            });
            $grid->address('Address');
            $grid->class('Class');

            $grid->column('location.latitude');
            $grid->column('location.longitude');
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ComDep::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->company_id('Company id');
        $show->id_com_dep_admin('Id com dep admin');
        $show->service_type_id('Service type id');
        $show->deleted_at('Deleted at');
        $show->id_admin_del('Id admin del');
        $show->id_admin_add('Id admin add');
        $show->info('Info');
        $show->info_for_vp_admin('Info for vp admin');
        $show->address('Address');
        $show->info_station('Info station');
        $show->telephone('Telephone');
        $show->e_mail('E mail');
        $show->class('Class');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ComDep);

        $form->text('name', 'Name');
        $form->number('company_id', 'Company id');
        $form->number('id_com_dep_admin', 'Id com dep admin');
        $form->number('service_type_id', 'Service type id');
        $form->number('id_admin_del', 'Id admin del');
        $form->number('id_admin_add', 'Id admin add');
        $form->textarea('info', 'Info');
        $form->textarea('info_for_vp_admin', 'Info for vp admin');
        $form->text('address', 'Address');
        $form->textarea('info_station', 'Info station');
        $form->text('telephone', 'Telephone');
        $form->text('e_mail', 'E mail');
        $form->text('class', 'Class');

        return $form;
    }
}
