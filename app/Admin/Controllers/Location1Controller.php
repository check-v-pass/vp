<?php

namespace App\Admin\Controllers;

use \App\ComDep;
use App\Company;
use App\Http\Controllers\Controller;
use App\ServiceType;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LocationController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ComDep::class, function (Grid $grid) {
            $grid->disableCreateButton();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->id('Id');
            $grid->company_id('Company')->display(function ($id) {
                return Company::find($id)->name;
            });
            $grid->name('Department');
            $grid->address('Address');
            $grid->class('Class');

            $grid->column('location.latitude');
            $grid->column('location.longitude');
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ComDep::findOrFail($id));
        $show->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
        $show->id('Id');
        $show->name('Name');
        $show->company_id('Company id');
        $show->id_com_dep_admin('Id com dep admin');
        $show->id_admin_add('Id admin add');
        $show->info('Info');
        $show->info_for_vp_admin('Info for vp admin');
        $show->address('Address');
        $show->info_station('Info station');
        $show->telephone('Telephone');
        $show->e_mail('E mail');
        $show->class('Class');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(ComDep::class, function (Form $form) {
            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableDelete();
            });
            $form->display('name', 'Department');
            $form->display('address', 'Address');
            $form->display('class', 'Class');
            $form->divider();
            $form->text('location.label')->rules('required|min:3|max:150');
            $form->textarea('location.info')->rules('required|min:3 ');
            $form->decimal('location.longitude')->attribute(['id'=>'lng'])->default(-74.005974);
            $form->decimal('location.latitude')->attribute(['id'=>'lat'])->default(40.712776);


            $th=<<<EOT
                <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.min.js'></script>
                <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css' type='text/css' />
                <style>
                    #geocoder-container > div {
                        min-width:50%;
                        margin-left:25%;
                    }
                </style>
                <div id='map' ></div>
                
                <script>
                    mapboxgl.accessToken = 'pk.eyJ1IjoibXVuaXIxMzU3IiwiYSI6ImNqbnU1dWJ4dzBnYWoza3J3ZXdmcjZ6ZzAifQ.jqf5sLoUrIjQ70YloZ1ttg';
                    var map = new mapboxgl.Map({
                        container: 'map',
                        style: 'mapbox://styles/mapbox/streets-v9',
                        center: [document.getElementById('lng').value, document.getElementById('lat').value],
                        zoom: 13
                    });
                    var marker = new mapboxgl.Marker({
                        draggable: true
                    })
                    
                    function onDragEnd() {
                        var lngLat = marker.getLngLat();
                        document.getElementById('lng').value=lngLat.lng;
                        document.getElementById('lat').value=lngLat.lat;
                    }
        
                    marker.on('dragend', onDragEnd);
                    
                    var geocoder = new MapboxGeocoder({
                        accessToken: mapboxgl.accessToken
                    });
                    
                    map.addControl(geocoder);
                    
                    // After the map style has loaded on the page, add a source layer and default
                    // styling for a single point.
                    map.on('load', function() {
                        marker.setLngLat([document.getElementById('lng').value, document.getElementById('lat').value])
                        marker.addTo(map);
                        map.addSource('single-point', {
                            "type": "geojson",
                            "data": {
                                "type": "FeatureCollection",
                                "features": []
                            }
                        });
                    
                        map.addLayer({
                            "id": "point",
                            "source": "single-point",
                            "type": "circle",
                            "paint": {
                                "circle-radius": 10,
                                "circle-color": "#007cbf"
                            }
                        });
                    
                        // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
                        // makes a selection and add a symbol that matches the result.
                        geocoder.on('result', function(ev) {  
                            
                            console.log(ev.result.geometry);    
                            
                            map.getSource('single-point').setData(ev.result.geometry);
                            document.getElementById('lng').value=ev.result.geometry.coordinates[0];
                            document.getElementById('lat').value=ev.result.geometry.coordinates[1];
                                
                            marker.setLngLat([ev.result.geometry.coordinates[0], ev.result.geometry.coordinates[1]]);
                            marker.addTo(map);
                        });
                    });
                </script>

EOT;
            $form->html($th);

            $form->embeds('info_json', function ($form) {
                $form->icon('icon_1');
                $form->text('text_1')->rules('max:240');

                $form->icon('icon_2');
                $form->text('text_2')->rules('max:240');

                $form->icon('icon_3');
                $form->text('text_3')->rules('max:240');
            });

        });
    }

}
