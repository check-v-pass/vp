<?php

namespace App\Admin\Controllers;

use App\Company;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\ComCity;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Facades\Admin;

class CompanyController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */


    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Company::class, function (Grid $grid) {
            $grid->id('Id')->sortable();
            $grid->name('Name');
            $grid->info('Info');
            $grid->info_for_vp_admin('Info for vp admin');
            $grid->id_com_admin('Id com admin');
            $grid->telephone('Telephone');
            $grid->web_site('Web site');
            $grid->created_at('Created at');
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->id_admin_add('Id admin add');
        $show->info('Info');
        $show->info_for_vp_admin('Info for vp admin');
        $show->id_com_admin('Id com admin');
        $show->telephone('Telephone');
        $show->web_site('Web site');
        $show->e_mail('E mail');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Company);
        $options = Administrator::where('id', '!=', Admin::user()->id)->get()->pluck('name', 'id');
        $form->select('id_com_admin','company_admin')->options($options)->rules('required');

       // $stateEdit=explode('/',url()->current());
        $form->text('name', 'Company name')->rules(function ($form) {
            if (!$id = $form->model()->id) {
                return 'required|unique:companies,name|min:3|max:200';
            }else return 'required|min:3|max:200|unique:companies,name,'.$id;

        });
        $form->mobile('telephone', 'Telephone')->rules(function ($form) {
            if (!$id = $form->model()->id) {
                return 'required|unique:companies,telephone';
            }else return 'required|unique:companies,telephone,'.$id;

        });
        $form->email('e_mail', 'E mail')->rules(function ($form) {
            if (!$id = $form->model()->id) {
                return 'required|unique:companies,e_mail|min:5|max:190';
            }else return 'required|min:3|max:190|unique:companies,e_mail,'.$id;

        });
        $form->url('web_site', 'Web site')->rules('min:3|max:200');
        $form->textarea('info', 'Info');
        $form->textarea('info_for_vp_admin', 'Info for vp admin');
        $form->saving(function (Form $form) {
            $form->model()->id_admin_add=Admin::user()->id;
        });

        return $form;
    }
    protected $optionCity="";
    public function opCity(){
        if(empty($this->optionCity)) $this->optionCity=ComCity::all()->pluck('name', 'id');
        return $this->optionCity;
    }
}
