<?php

namespace App\Admin\Controllers;

use App\Client;
use App\Country;
use App\Defrayment;
use App\Http\Controllers\Controller;
use App\Insurance;
use App\Product;
use App\Sale;
use App\SaleProduct;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class SaleProductClientController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return redirect('/admin/client_cart/create');
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $this->option();


        $form = new Form(new Client);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableList();
            $tools->disableView();
            $tools->disableDelete();
        });
        $insurancepercent=Insurance::all()->first()->percent;
        $form->text('first_name', 'First name');
        $form->text('second_name', 'Second name');
        $form->text('company', 'Company');
        $form->text('address_line_1', 'Address line 1');
        $form->text('address_line_2', 'Address line 2');
        $form->text('city', 'City');
        $form->mobile('telephone', 'Telephone');
        $form->email('e_mail', 'E mail')->rules('required|unique:clients,e_mail');
        $form->select('country_id', 'Country')->options(Country::all()->pluck('name','id'));
        $form->html('
                    <span id="copy1">
                        <div class="col-md-5" >
                             <label>Select product</label>
                             <select name = "product[]" class = "form-control product" onchange="totalSum()">'.$this->option_prod .'</select>
                        </div>
                        <div class="col-md-2">
                            <label>Adult</label>
                            <input name = "adult[]" type = "number" min = "0" max = "999" class = "form-control adult" value="0" onchange="totalSum()">
                        </div>
                        <div class="col-md-2">
                            <label>Child</label>
                            <input name = "child[]" type = "number" min = "0" max = "999" class = "form-control child" value="0"  onchange="totalSum()">
                        </div>
                    </span>
                    <div class="col-lg-3">
                        <label>Add line</label><br>
                        <div class="form-group">
                            <button type="button" onclick = "addRow()" class="btn btn-sm btn-success col-lg-6" id="add-table-field"><i class="fa fa-plus"></i></button>
                            <button disabled type="button" class="btn btn-sm btn-danger table-field-remove col-lg-6" id="add-table-field"><i class="fa fa-trash"></i></button>
                        </div>                        
                    </div>
                    <br>
                    <div id = "contentd">
                    </div><br>
                    <div id="total" class="form-group col-lg-12">
                        <div class="col-lg-8">
                            <input type="checkbox" name="insurance" class=""  onclick="insurancejs(this)">
                            <label>'.$insurancepercent.'%  '.'Insurance</label>
                        </div>                       
                        <div class="col-lg-4">
                            <label>TOTAL:<span id="labeltotal">0</span></label>   
                        </div>   
                        <script>
                            var prod=['.$this->ar_js_prod.'];
                            var sum=0;
                            function totalSum() {                                
                              var adult=document.getElementsByClassName("adult");
                              var child=document.getElementsByClassName("child");
                              var selectProd=document.getElementsByClassName("product");
                              var id;
                              sum=0;
                              for (var i = 0; i < selectProd.length; i++) {                                  
                                  id=$.inArray(selectProd[i].value, prod[0]);;
                                  sum=sum+adult[i].value*prod[1][id];
                                  sum=sum+child[i].value*prod[2][id];                                  
                              }                                    
                              document.getElementById("labeltotal").innerHTML=sum;
                            }
                            function insurancejs(checkbox) {
                                if (checkbox.checked){
                                    document.getElementById("labeltotal").innerHTML=sum+(sum*'.$insurancepercent.'/100);
                                }else{                                    
                                    document.getElementById("labeltotal").innerHTML=sum;
                                }
                              
                            } 
                        </script>                    
                    </div>
                    ',
                    'Buy product');

        $form->saving(function ($form) {
            if(count($form->product)<1){
                $this->er_message=$this->er_message.'Product select error empty:'.count($form->product);
                return $this->return_error($this->er_message);
            }
            foreach ($form->product as $item){
                $id=Product::where('name',$item)->first()->id;
                if(!$id) $this->er_message=$this->er_message.'Product select error';
                else array_push($this->prod,$id);
            }
            if(!empty($er_message)) {
                return $this->return_error($this->er_message);
            }
            $sum=0;
            $check_a_c=0;
            for ($i=0;$i<count($form->product);$i++){
                $item_a=$form->adult[$i];
                $item_c=$form->child[$i];
                // if all massiv zero
                $sum=$item_a*1+$item_c*1+$sum;

                if($item_a<0||$item_a>999)$check_a_c++;
                if($item_c<0||$item_c>999)$check_a_c++;
            }
            if($sum<=0||$check_a_c>0){
                $this->er_message=$this->er_message.'Count adult or child type wrong'.$check_a_c;
                return $this->return_error($this->er_message);
            }
            $this->formProduct=$form->product;
            $this->formAdult=$form->adult;
            $this->formChild=$form->child;
            unset($form->product);
            unset($form->adult);
            unset($form->child);
            DB::beginTransaction();
        });
        $form->saved(function (Form $form) {
            $def_insurance_id=0;
            if(isset($form->incurance)){
                $def_insurance_id=Insurance::all()->first()->id;
            }
            try{
                $defrayment_id=Defrayment::create([
                    'client_id'=>$form->model()->id,
                    'paid'=>true,
                    'insurance_id'=>$def_insurance_id
                ])->id;

                for ($i=0;$i<count($this->formProduct);$i++){
                    if($this->formAdult[$i]!=0){
                        SaleProduct::create([
                            'product_id'=>$this->prod[$i],
                            'client_id'=>$form->model()->id,
                            'defrayment_id'=>$defrayment_id,
                            'adult_child'=>1,
                            'count'=>$this->formAdult[$i]
                        ]);
                        for($k=0;$k<$this->formAdult[$i];$k++){
                            $prodSelect=Product::find($this->prod[$i]);
                            $attractionProd=$prodSelect->attraction;
                            $dayProd=$prodSelect->days_after_activation;
                            Sale::create([
                                'product_id'=>$this->prod[$i],
                                'defrayment_id'=>$defrayment_id,
                                'count_visit'=>0,
                                'limit'=>$attractionProd,
                                'days'=>$dayProd,
                                'qrcode_id'=>md5(str_random(8).$this->getmt())
                            ]);
                        }
                    }
                    if($this->formChild[$i]!=0){
                        SaleProduct::create([
                            'product_id'=>$this->prod[$i],
                            'client_id'=>$form->model()->id,
                            'defrayment_id'=>$defrayment_id,
                            'adult_child'=>2,
                            'count'=>$this->formChild[$i]
                        ]);
                        for($k=0;$k<$this->formChild[$i];$k++){
                            $prodSelect=Product::find($this->prod[$i]);
                            $attractionProd=$prodSelect->attraction;
                            $dayProd=$prodSelect->days_after_activation;
                            Sale::create([
                                'product_id'=>$this->prod[$i],
                                'defrayment_id'=>$defrayment_id,
                                'count_visit'=>0,
                                'limit'=>$attractionProd,
                                'days'=>$dayProd,
                                'qrcode_id'=>md5(str_random(8).$this->getmt())
                            ]);
                        }
                    }

                }

                DB::commit();
            }catch (Exception $e) {
                DB::rollBack();
                $this->er_message=$this->er_message.' '. ($form->model()->id) .' '.$e;
                return $this->return_error($this->er_message);
            }

        });
        return $form;
    }

    public $formProduct=[];
    public $formAdult=[];
    public $formChild=[];
    public $er_message='';
    public function return_error($message){
        $error = new MessageBag([
            'title'   => 'ERROR',
            'message' => $message,
        ]);
        return back()->with(compact('error'))->withInput();
    }
    public $option_prod="";
    public $ar_js_prod='';
    public function option(){
        $prod=Product::all('name','price_adult','price_child');
        $price_ad='[';
        $price_ch='[';
        $price_pr='[';
        foreach ($prod as $item){
            $this->option_prod=$this->option_prod.'<option>'.$item->name.'</option>';
            $price_pr=$price_pr.'"'.$item->name.'",';
            $price_ch=$price_ch.$item->price_child.',';
            $price_ad=$price_ad.$item->price_adult.',';
        }
        $price_ad=$price_ad.'],';
        $price_ch=$price_ch.'],';
        $price_pr=$price_pr.'],';
        $this->ar_js_prod=$price_pr.$price_ad.$price_ch;
    }

    public $prod=array();

    private function getmt()
    {
        //return array_sum(explode(" ", microtime()));
        usleep(1);

        return (microtime(TRUE)-1513728000)*1000000;
    }
}
