<?php

namespace App\Admin\Controllers;

use \App\ComDep;
use App\Company;
use App\Http\Controllers\Controller;
use App\ServiceType;
use App\Trav;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class TravController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public $editID=0;
    public function edit($id, Content $content)
    {
        $this->editID=$id;
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ComDep);

        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });

        $grid->tools(function (Grid\Tools $tools) {
            $tools->batch(function (Grid\Tools\BatchActions $actions) {
                $actions->disableDelete();
            });
        });

        $grid->id('Id');
        $grid->company_id('Company')->display(function ($id){
            return Company::find($id)->name;
        });
        $grid->name('Department');
        $grid->service_type_id('Service type')->display(function ($id){
            return ServiceType::find($id)->name;
        });
        $grid->address('Address');
        $grid->class('Class');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ComDep::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->company_id('Company id');
        $show->id_com_dep_admin('Id com dep admin');
        $show->service_type_id('Service type id');
        $show->id_admin_add('Id admin add');
        $show->info('Info');
        $show->info_for_vp_admin('Info for vp admin');
        $show->address('Address');
        $show->info_station('Info station');
        $show->telephone('Telephone');
        $show->e_mail('E mail');
        $show->class('Class');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ComDep);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
            $tools->disableDelete();
        });
        $form->display('id', 'ID');
        $form->display('name', 'Department');
        $form->display('address', 'Address');
        $form->display('class', 'Class');
        $form->hasMany('travs', function(Form\NestedForm $form){
            $form->timeRange('time_start','time_end','Range Time')->rules('required');
            $form->switch('mon','Monday')->setWidth(1,1);
            $form->switch('tue','Tuesday')->setWidth(1,1);
            $form->switch('wed','Wednesday')->setWidth(1,1);
            $form->switch('thu','Thursday')->setWidth(1,1);
            $form->switch('fri','Friday')->setWidth(1,1);
            $form->switch('sat','Saturday')->setWidth(1,1);
            $form->switch('sun','Sunday')->setWidth(1,1);
        });

        return $form;
    }
}
