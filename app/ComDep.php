<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComDep extends Model
{
    use SoftDeletes;
    protected $guarded=['id'];
    public function travs(){
        return $this->hasMany(Trav::class);
    }
    public function location(){
        return $this->hasOne(Location::class);
    }
    public function company()
    {
        return $this->belongsTo(Company::class );
    }

    public function com_services(){
        return $this->hasMany(ComService::class);
    }
    //
}
