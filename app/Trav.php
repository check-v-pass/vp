<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trav extends Model
{
    use SoftDeletes;
    public $guarded=['id'];

    public function com_dep(){
        return $this->belongsTo(ComCity::class );
    }
    //
}
