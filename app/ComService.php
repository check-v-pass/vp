<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComService extends Model
{
    use SoftDeletes;

    public function com_deps()
    {
        return $this->belongsTo(ComDep::class,'com_dep_id','id');
    }

    public function service_type(){
        return $this->hasOne(ServiceType::class,'id','service_type_id');
    }
    //
}
