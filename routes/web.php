<?php
use Illuminate\Support\Facades\URL;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//URL::forceSchema('https');
Route::get('/', function () {
    return view('welcome');
});
Route::get('/{city_id}/map','MapBoxController@index', function ($city_id) {})->name('map');
